<?php

namespace Drupal\jsonapi_include_lb\Plugin\Field\FieldType;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\layout_builder\LayoutEntityHelperTrait;

/**
 * Class for `my_field_name` computed field.
 */
class LayoutBuilderBlocksFieldComputed extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  use LayoutEntityHelperTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    DataDefinitionInterface $definition,
    $name,
    TypedDataInterface $parent
  ) {
    parent::__construct($definition, $name, $parent);
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * Compute the values.
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    $sections = $this->getEntitySections($entity);
    $block_revision_ids = [];
    foreach ($sections as $section) {
      foreach ($section->getComponents() as $component) {
        $component_data = $component->toArray();
        if (isset($component_data['configuration']['block_revision_id']) &&
          !empty($component_data['configuration']['block_revision_id'])) {
          $block_revision_ids[] = $component_data['configuration']['block_revision_id'];
        }
      }
    }
    if (count($block_revision_ids) > 0) {
      $block_content_storage = $this->entityTypeManager->getStorage(
        'block_content'
      );
      $block_contents = $block_content_storage->loadByProperties(
        ['revision_id' => $block_revision_ids]
      );
      $key = 0;
      foreach ($block_contents as $block_content) {
        $this->list[$key] = $this->createItem($key, $block_content->id());
        ++$key;
      }
    }
  }

}
