<?php

namespace Drupal\jsonapi_include_lb;

use Drupal\Component\Serialization\Json;
use Drupal\jsonapi_include\JsonapiParseInterface;

/**
 * Class JsonapiParseLb.
 *
 * Jsonapi Layout Builder include Parser.
 *
 * @package Drupal\jsonapi_include
 */
class JsonapiParseLb implements JsonapiParseInterface {

  /**
   * The root parser.
   *
   * @var \Drupal\Component\Serialization\JsonapiParseInterface
   */
  protected $rootParser;

  /**
   * JsonapiParseLb constructor.
   *
   * @param \Drupal\jsonapi_include\JsonapiParseInterface $root_parser
   *   The decorated parser.
   */
  public function __construct(JsonapiParseInterface $root_parser) {
    $this->rootParser = $root_parser;
  }

  /**
   * Parse Resource.
   *
   * @param array|mixed $item
   *   The data for resolve.
   *
   * @return array
   *   Result.
   */
  protected function parseResource($item) {
    $output = $item;
    if (isset($item['layout_builder__blocks'])
    && isset($item['layout_builder__layout'])
    ) {
      $sections = & $output['layout_builder__layout'];
      $layout_builder_blocks = $item['layout_builder__blocks'];
      $layout_builder_block_map = [];
      foreach ($layout_builder_blocks as $block) {
        $layout_builder_block_map[$block['id']] = $block;
      }
      foreach ($sections as &$section) {
        if (isset($section['components'])) {
          foreach ($section['components'] as &$component) {
            if (isset($component['configuration']['block_revision_id'])) {
              $block_uuid = $component['configuration']['uuid'];
              $component['block'] = $layout_builder_block_map[$block_uuid] ?? NULL;
            }
          }
        }
      }
      unset($output['layout_builder__blocks']);
    }

    return $output;
  }

  /**
   * Parse json api.
   *
   * @param string|object $response
   *   The response data from jsonapi.
   *
   * @return mixed
   *   Parse jsonapi data.
   */
  public function parse($response) {
    $parsed_content = $this->rootParser->parse($response);
    return $this->parseBlocks($parsed_content);
  }

  /**
   * Check array is assoc.
   *
   * @param array $arr
   *   The array.
   *
   * @return bool
   *   Check result.
   */
  protected function isAssoc(array $arr) {
    if ([] === $arr) {
      return FALSE;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

  /**
   * Parse json api.
   *
   * @param string|object $response
   *   The response data from jsonapi.
   *
   * @return mixed
   *   Parse jsonapi data.
   */
  protected function parseBlocks($response) {
    $json = Json::decode($response);
    if (isset($json['errors']) || empty($json['data'])) {
      return $json;
    }
    if (!$this->isAssoc($json['data'])) {
      foreach ($json['data'] as $item) {
        $data[] = $this->parseResource($item);
      }
    }
    else {
      $data = $this->parseResource($json['data']);
    }
    $json['data'] = $data;
    return Json::encode($json);
  }

}
